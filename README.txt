### =============================== Mars Rover Program ===============================

### Description
-------------
The program is used to send data to a rovers in mars to facilitate their navigation system.
The rover understands the cardinal points and can face either East (E), West (W)
North (N) or South (S).

The rover understands three commands
� M - Move one space forward in the direction it is facing
� R - rotate 90 degrees to the right
� L - rotate 90 degrees to the left

These commands will be executed by the
rover and its resulting location sent back to HQ

---------- How to Use the Program ---------------
### Directory Structure
-------------------

--marsrover
  --lib
    -marsrover.rb(MarsRover Class)
  --spec
    -marsrover_spec.rb(Program Test)
    -spec_helper.rb(RSpec Helper)
  -.rspec(RSpec Config File)
  -init.rb(Ruby Program Starting Point)
  -README.txt(Read Me Fle)
  -roverdata.txt(Rover Data File)

### Running the Program
--------------------
The program was written using Ruby 2.3.3, and the test module uses RSpec 3.6

To run the program, navigate to the main directory of the program using a command-line interface, and execute the following command
> ruby init.rb

### Running the Test
----------------
To run the Test navigate to the [spec] directory under [marsrover] using a command-line interface, and excute the following command
>rspec marsrover_spec.rb

Note: The test for the boundary has been skipped. To run the test

- Comment line 86, 96 and 98 in the marsorver.rb file
- Remove the x in "xit" in the begining of line 47 of the marsrover_spec.rb file

### Desing Decision and Code Correctness
-------------------------------------

The program follows recommended Object-oriented design practices, with code seperation, ensuring that private sensible data is encapsulated. The code is designed to be resuable and scalable.
I have ensure code correcteness by testing all the methods while developing and using a behaviour driven development framework (RSpec) to perform furter tests.

