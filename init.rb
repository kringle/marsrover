require './lib/marsrover.rb'


if File.exist?("./roverdata.txt")
	@lines=File.readlines("./roverdata.txt")
	
	@bounds
	@position
	@commands

	3.times{|x|
		if x == 0
				@bounds = @lines[x].split.map(&:to_i)
			elsif x ==1
					@position = @lines[x].split
			elsif x== 2
					@commands = @lines[x]
			else
    end
	}

		#Creates an instance of the MarsRover Object and Processes the commands to move the rover.
		rover  = MarsRover.new(@bounds, @position[0], @position[1], @position[2])
		puts rover.process_commands (@commands)
else
	puts 'Data file Not found'
end

