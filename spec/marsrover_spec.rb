require '../lib/marsrover'

describe 'MarsRover' do

    it 'allows checking the returned string (Position and Orientation) after proccessing the commands'  do
      rover = MarsRover.new([8,8],2,3,'E')
      expect(rover.process_commands('MMLMRMMRRMMLMLMM')).to eq('Position x: 6, Position y: 3, Orientation: E')
    end

    it 'allows checking for the value of the position hash after processing the commands' do
      #Checks if the last position from the program is matching the supplied position hash
      rover = MarsRover.new([8,8],2,3,'E')
      rover.process_commands('MMLMRMMRRMMLMR')
      expect(rover.instance_variable_get(:@location)).to eq({:x=>4, :y=>3})
    end

    it 'allows checking for the orientation after processing the commands' do

      #Checks if the last orientation from the program is matching the supplied orientatirn
      rover = MarsRover.new([8,8],2,3,'E')
      rover.process_commands('MMLMRMMRRMMLMR')
      expect(rover.instance_variable_get(:@orientation)).to eq('W')
    end

    it "allows checking if the valid_orientation instance variable matches the directions ['N', 'S', 'E', 'W'] Array " do

      #checks if the valid_orientation instance variable  matches the supplied valid orientations Array
      directions = ['N', 'S', 'E', 'W']

      rover = MarsRover.new([8,8],2,3,'E')
      rover.process_commands('MMLMRMMRRMMLMR')
      expect(rover.instance_variable_get(:@valid_orientations)).to match_array(directions)
    end

    it 'allows checking for the orientation change' do

      #Moving Left from the first orientation "North" should change to the orientation to the last position in the array
      rover = MarsRover.new([8,8],2,3,'N')
      expect{   rover.process_commands('L')}.to change{rover.instance_variable_get(:@orientation)}.from('N').to('W')

      #Moving Right from the last orientation value "West" should change to the orientation to the first in the array 'North'
      expect{   rover.process_commands('R')}.to change{rover.instance_variable_get(:@orientation)}.from('W').to('N')

    end


    xit "allows checking if the program raise an out of boundary error when the rover tries to move out of the field's boundaries" do
      #Skipped: Comments the Begin and Rescue end Lines in the (process_commands) method to run the test below


      #Eastern Boundary
      rover = MarsRover.new([6,6],4,3,'E')
      expect{rover.process_commands('MMLMMLRL')}.to raise_error(RoverOutOfBoundException,'Cannot move further East')

      #Northen Boundary
      rover = MarsRover.new([6,6],4,3,'S')
      expect{rover.process_commands('MRRMMMLMRMLRL')}.to raise_error(RoverOutOfBoundException,'Cannot move further North')

      #Western Boundary
      rover = MarsRover.new([6,6],0,3,'W')
      expect{rover.process_commands('MRRMMMLMRMLRL')}.to raise_error(RoverOutOfBoundException,'Cannot move further West')

      #Southern Boundary
      rover = MarsRover.new([6,6],1,1,'W')
      expect{rover.process_commands('MLMMMLMRMLRL')}.to raise_error(RoverOutOfBoundException,'Cannot move further South')

    end





end