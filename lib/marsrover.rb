class RoverOutOfBoundException < StandardError

  def initialize(msg)
    super
  end
end

class MarsRover
	@boundaries
	@location
	@orientation
	@valid_orientations 

	#Initilises the MarsRover Object
	def initialize(boundaries=[4,4], location_x=0, location_y=0, orientation='E')
		
		@boundaries = boundaries
		@location =  {:x=>location_x.to_i, :y=>location_y.to_i}
		@orientation = orientation
		@valid_orientations = %w(N E S W)

	end

	#Moves the rover forward or backward on the X or Y axis
	#Raises a RoverOutOfBoundException if the rover has reached the boundaries
	protected
	def move

		
		case @orientation
			when  'N'
				@location[:y] + 1 < @boundaries[1] ? @location[:y] +=1 : raise( RoverOutOfBoundException ,'Cannot move further North')
			when 'E'
				@location[:x] +1 < @boundaries[0] ? @location[:x] +=1 : raise( RoverOutOfBoundException ,'Cannot move further East')
			when 'S'
				@location[:y] -1  >= 0 ? @location[:y] -=1 : raise( RoverOutOfBoundException ,'Cannot move further South')
			when 'W'
				@location[:x] -1 >= 0 ? @location[:x] -=1:  raise( RoverOutOfBoundException ,'Cannot move further West')
		end

	end



	#Turn the rover 90 degrees to the left or to the right
	#Accepts an argument for the direction to turn to
	protected
	def turn(command)

		index = @valid_orientations.index(@orientation)
		
		case command 
		
			when 'L'
		
				if index == 0
					@orientation=  @valid_orientations[3]
				else
					@orientation = @valid_orientations[index-1]
				end

			
			when 'R'
				
				if index == 3
					@orientation  =  @valid_orientations[0]
				else
					@orientation= @valid_orientations[index+1]
				end

		end

	end

	#Processes the command for moving and turning
	#Return the current position and orientation of the rover after processing the commands
	public
	def process_commands (commands)

		commands = commands.split('')
		
		begin   RoverOutOfBoundException
		
			commands.each{
				|x|
				
				if x.upcase == 'M'
				
					move()
				
				elsif x.upcase == 'L' || x.upcase == 'R' 
					turn(x)
				end
			}
			
		rescue Exception => e
		  puts 'Command Error:  ' + e.message
		end
		
		return "Position x: #{@location[:x]}, Position y: #{@location[:y]}, Orientation: #{@orientation}"
	
	end

end
	